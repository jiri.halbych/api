class ActivityCard < ApplicationRecord
  has_many :user_cards
  has_many_attached :images
  has_many_attached :audios
  has_many_attached :others
  belongs_to :deck
  has_many :requirements, :foreign_key => "requirement_id"
end
