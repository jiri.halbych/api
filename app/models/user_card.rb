class UserCard < ApplicationRecord
  enum state: [:novy, :learning, :review, :relearning]
  belongs_to :user
  belongs_to :activity_card
  has_many :revlogs
end
