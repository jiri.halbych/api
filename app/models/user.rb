class User < ApplicationRecord
  include Argon2SecurePassword
  has_argon_password
  api_guard_associations refresh_token: 'refresh_tokens'
  has_many :refresh_tokens, dependent: :delete_all
  has_many :user_cards
  has_many_attached :temp_files
end
