class Requirement < ApplicationRecord
  belongs_to :activity_card_id, :class_name => "ActivityCard"
  belongs_to :requirement_id, :class_name => "ActivityCard"
end
