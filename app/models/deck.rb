class Deck < ApplicationRecord
  has_many :activity_cards
  belongs_to :course
end
