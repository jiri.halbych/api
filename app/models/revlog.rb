class Revlog < ApplicationRecord
  enum state: [:novy, :learning, :review, :relearning]
  belongs_to :user_card
end
