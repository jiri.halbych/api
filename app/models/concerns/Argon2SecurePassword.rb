module Argon2SecurePassword
  extend ActiveSupport::Concern
  MAX_PASSWORD_LENGTH_ALLOWED = 72

  class << self
    attr_accessor :time_cost
    attr_accessor :memory_cost
    attr_accessor :secret
  end
  self.time_cost = 2 # 1..10
  self.memory_cost = 16 # 1..31

  module ClassMethods
    def has_argon_password(attribute = :password, validations: true)
      include InstanceMethodsOnActivation.new(attribute)

      if validations
        include ActiveModel::Validations

        validate do |record|
          record.errors.add(attribute, :blank) unless record.public_send("#{attribute}_digest").present?
        end

        validates_length_of attribute, maximum: ActiveModel::SecurePassword::MAX_PASSWORD_LENGTH_ALLOWED
        validates_confirmation_of attribute, allow_blank: true
      end
    end
  end

  class InstanceMethodsOnActivation < Module
    def initialize(attribute)
      attr_reader attribute

      define_method("#{attribute}=") do |unencrypted_password|
        if unencrypted_password.nil?
          instance_variable_set("@#{attribute}", nil)
          self.public_send("#{attribute}_digest=", nil)
        elsif !unencrypted_password.empty?
          instance_variable_set("@#{attribute}", unencrypted_password)

          hasher = Argon2::Password.new(t_cost: Argon2SecurePassword.time_cost,
                                        m_cost: Argon2SecurePassword.memory_cost,
                                        secret: Argon2SecurePassword.secret)
          self.public_send("#{attribute}_digest=", hasher.create(unencrypted_password))
        end
      end

      define_method("#{attribute}_confirmation=") do |unencrypted_password|
        instance_variable_set("@#{attribute}_confirmation", unencrypted_password)
      end

      define_method("authenticate_#{attribute}") do |unencrypted_password|
        attribute_digest = public_send("#{attribute}_digest")
        attribute_digest.present? && Argon2::Password.verify_password(unencrypted_password,attribute_digest) && self
      end

      alias_method :authenticate, :authenticate_password if attribute == :password
    end
  end
end