class OptimizationJob < ActiveJob::Base
  self.queue_adapter = :resque
  queue_as :optimize

  def perform(user_id)
    user = User.find(user_id)
    File.open("revlog.csv", "w") { |f| f.write "review_time,card_id,review_rating,review_duration,review_state\n" }
    user.user_cards.each do |user_card|
      user_card.revlogs.each do |revlog|
        File.write("revlog.csv","#{revlog.review_time},#{user_card.id},#{revlog.rating.to_i},#{revlog.duration},#{revlog.state.to_i}\n", mode: "a")
      end
    end
    optimizer = PyCall.import_module("fsrs_optimizer")
    timezone = 'Europe/Prague'
    next_day_starts_at = 4
    revlog_start_date = "2006-10-05"
    optimizer = optimizer.Optimizer.new
    analysis = optimizer.create_time_series(timezone, revlog_start_date, next_day_starts_at)
    optimizer.define_model
    optimizer.pretrain(verbose: false)
    optimizer.train(verbose: false)
    user.update!(weights: optimizer.w.to_a)
  end
end
