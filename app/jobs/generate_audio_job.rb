require 'uri'
require 'net/http'
require 'json'
class GenerateAudioJob < ActiveJob::Base
  self.queue_adapter = :resque
  queue_as :generate_audio

  def perform(card_id, question, language, voice)
    card = ActivityCard.find(card_id)
    text = question ? card.question : card.answer

    uri = URI('https://api.speechactors.com/v1/generate')
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    req = Net::HTTP::Post.new(uri)
    req['Content-Type'] = 'application/json'
    req.body = {'locale' => language, 'vid' => voice, 'text' => text}.to_json
    req['Authorization'] = "Bearer ZU7KqVvgpPiQsf4L3hN4N4otNJjoPJ5pm98vhFka"
    res = https.request(req)
    t = Tempfile.new("temp_audio.mp3", binmode: true)
    t.write(res.body)
    t.close
    card.audios.attach(io: File.open(t.path), filename: "#{card_id}.mp3", content_type: 'audio/mpeg')
  end
end
