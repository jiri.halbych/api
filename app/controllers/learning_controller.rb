class LearningController < ApplicationController
  before_action :authenticate_and_set_user
  before_action only: :get_cards do
    ActiveStorage::Current.url_options = { protocol: "https://", host: "cognoro.xyz", port: 443 }
  end

  def get_cards
    deck_id = params[:deck]
    deck = Deck.find(deck_id)
    final_json = {cards: []}
    cards = deck.activity_cards.where(standalone: true).sort
    cards_copy = deck.activity_cards.where(standalone: true).sort
    cards.each do |card|
      requirements = Requirement.where(activity_card_id: card.id)
      requirements.each do |requirement|
        cards_copy.insert(cards_copy.find_index(card),ActivityCard.find(requirement.requirement_id))
      end
    end
    cards_copy.each do |card|
      audios = []
      card.audios.each do |audio|
        audios << audio.url(expires_in: 60.minutes)
      end
      card_json = {id: card.id, question: card.question, answer: card.answer, types: card.types, options: card.options, audios: audios}
      final_json[:cards] << card_json
    end
    render json: final_json, status: :ok
  end
end
