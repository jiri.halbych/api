class UsersController < ApplicationController
  before_action :authenticate_and_set_user

  def info
    user_cards = UserCard.where(user_id: current_user.id)
    repetitions = 0
    user_cards.each do |card|
      repetitions += Revlog.where(user_card_id: card.id).length
    end
    render json: {
      email: current_user.email, repetitions: repetitions
    }, status: :ok
  end

  def cards
    usercards = UserCard.where(user_id: current_user.id)
    final_json = {cards: []}
    usercards.each do |card|
      card_json = {
        id: card.activity_card.id,
        question: card.activity_card.question,
        answer: card.activity_card.answer,
        reps: card.reps,
        stability: card.stability,
        difficulty: card.difficulty,
        due: card.due,
        last: card.last_review
      }
      final_json[:cards] << card_json
    end
    render json: final_json, status: :ok
  end
end
