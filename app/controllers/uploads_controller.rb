require 'uri'
require 'net/http'
class UploadsController < ApplicationController
  before_action :authenticate_and_set_user

  def languages
    uri = URI("https://api.speechactors.com/v1/languages")
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    req = Net::HTTP::Get.new(uri)
    req['Content-Type'] = 'application/json'
    req['Authorization'] = "Bearer ZU7KqVvgpPiQsf4L3hN4N4otNJjoPJ5pm98vhFka"
    res = https.request(req)
    render json: res.body, status: :ok
  end

  def voices
    uri = URI("https://api.speechactors.com/v1/voices")
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    req = Net::HTTP::Get.new(uri)
    req['Content-Type'] = 'application/json'
    req['Authorization'] = "Bearer ZU7KqVvgpPiQsf4L3hN4N4otNJjoPJ5pm98vhFka"
    res = https.request(req)
    final_json = {data: []}
    JSON.parse(res.body)["data"].each do |voice|
      if voice["locale"] == params[:locale]
        final_json[:data] << voice
      end
    end
    render json: final_json, status: :ok
  end

  def upload_audio
    current_user.temp_files.attach(io: File.open(params[:file].tempfile.path), filename: params[:file].original_filename, content_type: 'audio/mpeg')
  end

  def remove
    current_user.temp_files.each do |file|
      if file.blob.filename == params[:file][:tmp]
        file.purge
        break
      end
    end
  end
end
