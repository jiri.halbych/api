require 'set'
class CardsController < ApplicationController
  before_action :authenticate_and_set_user
  before_action do
    ActiveStorage::Current.url_options = { protocol: "https://", host: "cognoro.xyz", port: 443 }
  end
  def course_cards
    decks = Deck.where(course_id: params[:course])
    final_json = {cards: []}
    decks.each do |deck|
      cards = ActivityCard.where(deck_id: deck.id)
      cards.each do |card|
        deck = Deck.find(card.deck_id)
        audios = []
        card.audios.each do |audio|
          audios << audio.url
        end
        card_json = {
          id: card.id,
          deck: card.deck_id,
          course: deck.course_id,
          question: card.question,
          answer: card.answer,
          types: card.types,
          options: card.options,
          audios: audios
        }
        final_json[:cards] << card_json
      end
    end
    render json: final_json, status: :ok
  end
  def delete
    card = ActivityCard.find(params[:card])
    card.destroy!
  end
  def all
    cards = ActivityCard.all
    final_json = {cards: []}
    cards.each do |card|
      deck = Deck.find(card.deck_id)
      audios = []
      card.audios.each do |audio|
        audios << audio.url
      end
      card_json = {
        id: card.id,
        deck: card.deck_id,
        course: deck.course_id,
        question: card.question,
        answer: card.answer,
        types: card.types,
        options: card.options,
        audios: audios
      }
      final_json[:cards] << card_json
    end
    render json: final_json, status: :ok
  end

  def update_dialogue(card)
    card.update!(deck_id: params[:deck], question: "Dialogue", answer: "Dialogue", types: params[:activity_types], options: {dialogue: []})
    options = card.options
    params[:dialogue].each do |phrase|
      options["dialogue"] << phrase
    end
    card.update!(options: options)
    card.audios.purge
    GenerateDialogueJob.perform_later(card.id)
  end
  def update
    card = ActivityCard.find(params[:card])
    if params[:activity_types].include?("dialogue")
      update_dialogue(card)
      return
    end
    card.update!(deck_id: params[:deck], question: params[:question], answer: params[:answer], types: params[:activity_types], options: {question_hints: {}, answer_hints: {}})
    if Requirement.where(activity_card_id: card.id).any?
      requirements = Requirement.where(activity_card_id: card.id)
      requirements.each do |requirement|
        requirement.destroy!
      end
      params[:requirements].each do |requirement|
        Requirement.create!(activity_card_id: card.id, requirement_id: requirement)
      end
    end
    options = card.options
    if card.types.include?("fill")
      options["fill"] = {}
      options["fill"]["correct"] = params[:fill_correct]
      options["fill"]["wrong"] = params[:fill_wrong]
    end
    params[:question_hints].each do |hint|
      options["question_hints"][hint['question']] = hint[:answer]
    end
    params[:answer_hints].each do |hint|
      options["answer_hints"][hint['question']] = hint[:answer]
    end
    if params.has_key?(:play_audio)
      if params[:play_audio].include?("answer")
        options["play_at_end"] = true
      end
    else
      options["play_at_end"] = false
    end
    card.update!(options: options)
    card.audios.purge
    if params.has_key?(:generate_audio)
      audios = params[:generate_audio]
      if audios.include?('question')
        GenerateAudioJob.perform_later(card.id, true, "cs-CZ", "cs-CZ-AntoninNeural")
      end
      if audios.include?('answer')
        GenerateAudioJob.perform_later(card.id, false, card.deck.course.locale, params[:voice])
      end
    end
    if params.include?('question_audios')
      params[:question_audios].values.each do |data|
        current_user.temp_files.each do |temp_file|
          if data[:audio][:originalName] == temp_file.blob.filename.to_s
            card.audios.attach(temp_file.blob)
          end
        end
      end
    end
    if params.include?('answer_audios')
      params[:answer_audios].values.each do |data|
        current_user.temp_files.each do |temp_file|
          if data[:audio][:originalName] == temp_file.blob.filename.to_s
            card.audios.attach(temp_file.blob)
          end
        end
      end
    end
    current_user.temp_files.detach
  end

  def handle_dialogue
    card = ActivityCard.create!(deck_id: params[:deck], question: "Dialogue", answer: "Dialogue", types: params[:activity_types], options: {dialogue: []})
    options = card.options
    params[:dialogue].each do |phrase|
      options["dialogue"] << phrase
    end
    card.update!(options: options)
    GenerateDialogueJob.perform_later(card.id)
  end

  def create
    if params[:activity_types].include?("dialogue")
      handle_dialogue
      return
    end
    card = ActivityCard.create!(deck_id: params[:deck], question: params[:question], answer: params[:answer], types: params[:activity_types], standalone: params[:standalone], options: {question_hints: {}, answer_hints: {}})
    params[:requirements].each do |requirement|
      Requirement.create!(activity_card_id: card.id, requirement_id: requirement)
    end
    options = card.options
    if card.types.include?("fill")
      options["fill"] = {}
      options["fill"]["correct"] = params[:fill_correct]
      options["fill"]["wrong"] = params[:fill_wrong]
    end
    params[:question_hints].each do |hint|
      options["question_hints"][hint['question']] = hint[:answer]
    end
    params[:answer_hints].each do |hint|
      options["answer_hints"][hint['question']] = hint[:answer]
    end
    if params.has_key?(:play_audio)
      if params[:play_audio].include?("answer")
        options["play_at_end"] = true
      end
    else
      options["play_at_end"] = false
    end
    card.update!(options: options)
    if params.has_key?(:generate_audio)
        audios = params[:generate_audio]
        if audios.include?('question')
          GenerateAudioJob.perform_later(card.id, true, "cs-CZ", "cs-CZ-AntoninNeural")
        end
        if audios.include?('answer')
          GenerateAudioJob.perform_later(card.id, false, card.deck.course.locale, params[:voice])
        end
      end
      if params.include?('question_audios')
        params[:question_audios].values.each do |data|
          current_user.temp_files.each do |temp_file|
            if data[:audio][:originalName] == temp_file.blob.filename.to_s
              card.audios.attach(temp_file.blob)
            end
          end
        end
      end
    if params.include?('answer_audios')
      params[:answer_audios].values.each do |data|
          current_user.temp_files.each do |temp_file|
            if data[:audio][:originalName] == temp_file.blob.filename.to_s
              card.audios.attach(temp_file.blob)
            end
          end
        end
    end
    current_user.temp_files.detach
  end
end
