class ReviewsController < ApplicationController
  before_action :authenticate_and_set_user
  before_action only: :give_cards do
    ActiveStorage::Current.url_options = { protocol: "https://", host: "cognoro.xyz", port: 443 }
  end

  def reviews
    due_cards = UserCard.where('due <= ?', Time.now).where(user_id: current_user.id).length
    render json: { reviews: due_cards }, status: :ok
  end

  def give_cards
    due_cards = UserCard.where('due <= ?', Time.now).where(user_id: current_user.id).order('due DESC').limit(10)
    if due_cards.nil?
      render json: { cards: [
        id: 0,
        question: 0,
        answer: 0,
        type: 0
      ]}, status: :ok
    else
      final_json = {cards: []}
      due_cards.each do |card|
        audios = []
        card.activity_card.audios.each do |audio|
          audios << audio.url(expires_in: 60.minutes)
        end
        card_json = {
          id: card.activity_card.id,
          question: card.activity_card.question,
          answer: card.activity_card.answer,
          types: card.activity_card.types,
          options: card.activity_card.options,
          audios: audios
        }
        final_json[:cards] << card_json
      end
      render json: final_json, status: :ok
    end
  end

  def load_card(card, details)
    card.stability = details.stability
    card.difficulty = details.difficulty
    card.last_review = details.last_review
    card.elapsed_days = details.elapsed_days
    card.scheduled_days = details.scheduled_days
    card.due = details.due
    card.lapses = details.lapses
    card.reps = details.reps
    case details.state
    when "novy"
      card.state = State::NEW
    when "learning"
      card.state = State::LEARNING
    when "review"
      card.state = State::REVIEW
    when "relearning"
      card.state = State::RELEARNING
    end
  end

  def repeat
    activity_card = params[:activity_card]
    grade = params[:grade]
    f = FSRS.new
    f.p.w = current_user.weights
    card = Card.new
    card_record = UserCard.where(activity_card_id: activity_card, user_id: current_user.id)
    exists = false
    if card_record.any?
      if card_record.first!.due > Time.now
        return
      end
      exists = true
      details = card_record.first
      load_card(card, details)
    end
    scheduled_cards = f.repeat(card, Time.now)
    new_card = scheduled_cards[grade].card
    review_log = scheduled_cards[grade].review_log
    if exists
      details.update!(stability: new_card.stability, difficulty: new_card.difficulty, last_review: new_card.last_review, scheduled_days: new_card.scheduled_days,
                      elapsed_days: new_card.elapsed_days, due: new_card.due, lapses: new_card.lapses, reps: new_card.reps, state: new_card.state)
      Revlog.create!(user_card_id: details.id, rating: review_log.rating, state: review_log.state, duration: params[:duration], review_time: review_log.review)
    else
      user_card = UserCard.create!(user_id: current_user.id, activity_card_id: activity_card, stability: new_card.stability, difficulty: new_card.difficulty, last_review: new_card.last_review,
                      elapsed_days: new_card.elapsed_days, scheduled_days: new_card.scheduled_days, due: new_card.due, lapses: new_card.lapses, reps: new_card.reps, state: new_card.state)
      Revlog.create!(user_card_id: user_card.id, rating: review_log.rating, state: review_log.state, duration: params[:duration], review_time: review_log.review)
    end
    #OptimizationJob.perform_later(current_user.id)
  end
end
