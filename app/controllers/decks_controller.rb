class DecksController < ApplicationController
  before_action :authenticate_and_set_user

  def all
    decks = Deck.all
    final_json = {data: []}
    decks.each do |deck|
      final_json[:data] << {id: deck.id, course: deck.course_id, name:deck.name, description: deck.description}
    end
    render json: final_json, status: :ok
  end
  def create
    Deck.create!(course_id: params[:course], name: params[:name], description: params[:description], audio: params[:audio])
  end

  def update
    deck = Deck.find(params[:deck])
    deck.update!(course_id: params[:course], name: params[:name], description: params[:description], audio: params[:audio])
  end

  def delete
    deck = Deck.find(params[:deck])
    deck.destroy!
  end
  def audio_decks
    decks = Deck.where(audio: true, course_id: params[:course])
    final_json = {data: []}
    finished = true
    decks.each do |deck|
      deck.activity_cards.each do |card|
        if UserCard.where(user_id: current_user.id, activity_card_id: card.id).empty?
          finished = false
        end
      end
      final_json[:data] << {id: deck.id, name:deck.name, description: deck.description, finished: finished}
    end
    render json: final_json, status: :ok
  end
  def decks
    decks = Deck.where(course_id: params[:course])
    final_json = {data: []}
    finished = true
    decks.each do |deck|
      deck.activity_cards.each do |card|
        if UserCard.where(user_id: current_user.id, activity_card_id: card.id).empty?
          finished = false
        end
      end
      final_json[:data] << {id: deck.id, name:deck.name, description: deck.description, finished: finished}
    end
    render json: final_json, status: :ok
  end
end
