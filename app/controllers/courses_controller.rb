class CoursesController < ApplicationController
  before_action :authenticate_and_set_user
  def create
    Course.create!(name: params[:name], locale: params[:locale])
  end

  def update
    course = Course.find(params[:course])
    course.update!(name: params[:name], locale: params[:locale])
  end

  def delete
    course = Course.find(params[:course])
    course.destroy!
  end

  def courses
    courses = Course.all
    final_json = {data: []}
    courses.each do |course|
      final_json[:data] << {id: course.id, name:course.name, locale: course.locale}
    end
    render json: final_json, status: :ok
  end
end
