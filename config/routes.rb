Rails.application.routes.draw do
  require 'resque/server'
  mount Resque::Server, at: '/jobs'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  api_guard_routes for: 'users'

  match '*all', controller: 'application', action: 'cors_preflight_check', via: [:options]

  post "/repeat", to: "reviews#repeat"
  post "/card/create", to: "cards#create"
  get "/user/info", to: "users#info"
  get "/user/cards", to: "users#cards"
  get "/reviews", to: "reviews#reviews"

  get "/review", to: "reviews#give_cards"
  get "/learn", to: "learning#get_cards"
  post "/upload/audio", to: "uploads#upload_audio"
  post "/upload/remove", to: "uploads#remove"
  get "/languages", to: "uploads#languages"
  get "/voices", to: "uploads#voices"
  get "/decks", to: "decks#decks"
  get "/decks/audio", to: "decks#audio_decks"
  get "/courses", to: "courses#courses"
  post "/deck/create", to: "decks#create"
  post "/course/create", to: "courses#create"

  post "/course/update", to: "courses#update"
  delete "/course/delete", to: "courses#delete"

  post "/deck/update", to: "decks#update"
  delete "/deck/delete", to: "decks#delete"
  get "/deck/all", to: "decks#all"

  post "/card/update", to: "cards#update"
  delete "/card/delete", to: "cards#delete"
  get "/card/all", to: "cards#all"
  get "/card/course", to: "cards#course_cards"
end