class CreateRevlogs < ActiveRecord::Migration[7.0]
  def change
    create_table :revlogs do |t|
      t.references :user_card, null: false, foreign_key: {on_delete: :cascade}
      t.integer :state, default: 0
      t.integer :rating
      t.timestamp :review_time
      t.integer :duration
    end
  end
end
