class CreateRequirements < ActiveRecord::Migration[7.0]
  def change
    create_table :requirements do |t|
      t.references :activity_card, foreign_key: { to_table: 'activity_cards', on_delete: :cascade }
      t.references :requirement, foreign_key: { to_table: 'activity_cards', on_delete: :cascade }
    end
  end
end
