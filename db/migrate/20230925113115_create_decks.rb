class CreateDecks < ActiveRecord::Migration[7.0]
  def change
    create_table :decks do |t|
      t.string :name
      t.string :description
      t.boolean :audio
      t.references :course, null: false, foreign_key: {on_delete: :cascade}
    end
  end
end
