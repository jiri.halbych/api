class CreateUserCards < ActiveRecord::Migration[7.0]
  def change
    create_table :user_cards do |t|
      t.float :stability, default: 0
      t.float :difficulty, default: 0
      t.datetime :due
      t.integer :elapsed_days, default: 0
      t.integer :scheduled_days, default: 0
      t.integer :reps, default: 0
      t.integer :lapses, default: 0
      t.integer :state, default: 0
      t.datetime :last_review
      t.references :user, null: false, foreign_key: {on_delete: :cascade}
      t.references :activity_card, null: false, foreign_key: {on_delete: :cascade}
    end
  end
end
