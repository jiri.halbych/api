class CreateActivityCards < ActiveRecord::Migration[7.0]
  def change
    create_table :activity_cards do |t|
      t.string :question
      t.string :answer
      t.string :types, array: true
      t.json :options
      t.references :deck, null: false, foreign_key: {on_delete: :cascade}
      t.boolean :standalone, default: true
    end
  end
end
