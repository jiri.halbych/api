# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2024_02_08_083056) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "activity_cards", force: :cascade do |t|
    t.string "question"
    t.string "answer"
    t.string "types", array: true
    t.json "options"
    t.bigint "deck_id", null: false
    t.index ["deck_id"], name: "index_activity_cards_on_deck_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.string "locale"
  end

  create_table "decks", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.boolean "audio"
    t.bigint "course_id", null: false
    t.index ["course_id"], name: "index_decks_on_course_id"
  end

  create_table "refresh_tokens", force: :cascade do |t|
    t.string "token"
    t.bigint "user_id", null: false
    t.datetime "expire_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["token"], name: "index_refresh_tokens_on_token", unique: true
    t.index ["user_id"], name: "index_refresh_tokens_on_user_id"
  end

  create_table "requirements", force: :cascade do |t|
    t.bigint "activity_card_id"
    t.bigint "requirement_id"
    t.index ["activity_card_id"], name: "index_requirements_on_activity_card_id"
    t.index ["requirement_id"], name: "index_requirements_on_requirement_id"
  end

  create_table "revlogs", force: :cascade do |t|
    t.bigint "user_card_id", null: false
    t.integer "state", default: 0
    t.integer "rating"
    t.datetime "review_time", precision: nil
    t.integer "duration"
    t.index ["user_card_id"], name: "index_revlogs_on_user_card_id"
  end

  create_table "user_cards", force: :cascade do |t|
    t.float "stability", default: 0.0
    t.float "difficulty", default: 0.0
    t.datetime "due"
    t.integer "elapsed_days", default: 0
    t.integer "scheduled_days", default: 0
    t.integer "reps", default: 0
    t.integer "lapses", default: 0
    t.integer "state", default: 0
    t.datetime "last_review"
    t.bigint "user_id", null: false
    t.bigint "activity_card_id", null: false
    t.index ["activity_card_id"], name: "index_user_cards_on_activity_card_id"
    t.index ["user_id"], name: "index_user_cards_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.float "weights", default: [0.4, 0.6, 2.4, 5.8, 4.93, 0.94, 0.86, 0.01, 1.49, 0.14, 0.94, 2.18, 0.05, 0.34, 1.26, 0.29, 2.61], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "activity_cards", "decks"
  add_foreign_key "decks", "courses"
  add_foreign_key "refresh_tokens", "users"
  add_foreign_key "requirements", "activity_cards"
  add_foreign_key "requirements", "activity_cards", column: "requirement_id"
  add_foreign_key "revlogs", "user_cards"
  add_foreign_key "user_cards", "activity_cards"
  add_foreign_key "user_cards", "users"
end
